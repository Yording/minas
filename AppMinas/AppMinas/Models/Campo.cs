//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppMinas.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Campo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Campo()
        {
            this.Estructura = new HashSet<Estructura>();
        }
    
        public long idCampo { get; set; }
        public string nombreCampo { get; set; }
        public int idTipoDato { get; set; }
        public string description { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Estructura> Estructura { get; set; }
        public virtual TipoDato TipoDato { get; set; }
    }
}
