﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AppMinas.Services;
using AppMinas.Listas;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AppMinas.Controllers
{
    public class MainController : ApiController
    {
        //Atributos
        private static HelperService helper;
        private static AuthService authService;
        private static FormService formService;
        private static ActivityService activityService;
        private static ConnectionService connectionService;
        private static string _token = string.Empty;

        public void insertarFormularios(List<Form> formsVicitrack)
        {
            // Insertar formularios
            formsVicitrack.ForEach(ele =>
            {
                // Verificamos si el formulario ya existe en la BD
                JObject response = JObject.Parse(formService.findFormGUID(ele.GUID));
                if (response.Value<Newtonsoft.Json.Linq.JToken>("value").Count<JToken>() == 0)
                {
                    //tenemos algunos formularios repetidos
                    formService.createForm(ele.GUID, ele.Title);
                }
            });
        }
        public async void Run()
        {
            Token authentication = JsonConvert.DeserializeObject<Token>(authService.getAuthentication());
            if (authentication.status == "OK")
            {
                // Se obtiene el token de acceso que devolvio la api
                string _token = authentication.AccessToken;

                // Instancias a utlizar
                activityService = new ActivityService(_token);
                formService = new FormService(_token);
                connectionService = new ConnectionService();

                // Se Obtiene una lista con todos los formularios alojados en la api
                List<Form> formsVicitrack = JsonConvert.DeserializeObject<List<Form>>(formService.getForms());

                //// Obtener las conexiones creadas
                //ResponseModel responseConnections = JsonConvert.DeserializeObject<ResponseModel>(connectionService.getConnectionsForms());
                //List<ConnectionModel> connectionsForms= JsonConvert.DeserializeObject<List<ConnectionModel>>(responseConnections.value.ToString());


                // Se obtiene una lista con las conexiones creadas en la BD.


                insertarFormularios(formsVicitrack);


                // Se obtiene la lista con todas las actividades de la api
                List<Activity> activiesVicitrack = JsonConvert.DeserializeObject<List<Activity>>(activityService.getActivity());

                // Foreach de todos las actividades(registros en los formularios)
                activiesVicitrack.ForEach(ele => {
                    DetailActivity activiesDetailVicitrack = JsonConvert.DeserializeObject<DetailActivity>(activityService.getDetailActivity(ele.GUID));
                    ResponseModel responseFindForm = JsonConvert.DeserializeObject<ResponseModel>(connectionService.findConnectionGUIDForms(activiesDetailVicitrack.FormGUID));
                    if (responseFindForm.value.ToString() != "[]")
                    {
                        activiesDetailVicitrack.Values.ForEach(value =>
                        {
                            Console.WriteLine("Del formulario {0} Campo {1} y Tipo de dato {2}", activiesDetailVicitrack.Title, value.apiId, value.Value.GetType());
                        });
                    }

                });



                // Prueba para convertir un JArray a una lista tipada
                //JObject activiesDetailVicitrack = JObject.Parse(activityService.getDetailActivity("e36e60d0-0fb8-4cb9-980a-e628f9524537"));
                //var jarr = activiesDetailVicitrack["Values"].Value<JArray>();
                //List<FormValues> lst = jarr.ToObject<List<FormValues>>();
                //var dasd = jarr[2]["Value"].Value<JArray>();
                //List<FormValues> lst2 = dasd.ToObject<List<FormValues>>();
                //var dasdsad = lst2[0].Value;

                // Prueba con Object para saber que puede ser varios tipos de datos
                // con GetType puedo saber el tipo de dato
                //object pad = 1;
                //object das = "dasd";
                //object ara = new Array [1, 23, 3];
                //Console.WriteLine("{0},{1},{2}",pad.GetType(), das.GetType(), ara.GetType());
            }
        }
        public bool get()
        {
            helper = new HelperService();
            authService = new AuthService(helper.getConfig("USER_VICITRACK"), helper.getConfig("PASSWORD_VICITRACK"));
            Run();
            return true;
        }
    }
}
