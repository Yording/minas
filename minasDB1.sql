USE master;

--Delete the minasDB database if it exists.  
IF EXISTS(SELECT * from sys.databases WHERE name='minasDB')  
BEGIN  
    DROP DATABASE minasDB; 
END  

--Create a new database called minasDB.  
CREATE DATABASE minasDB; 

USE minasDB; 

IF EXISTS(SELECT * FROM SYSOBJECTS WHERE XTYPE='U' AND NAME = 'Formulario')
	BEGIN
		DROP TABLE Formulario;
	END

	CREATE TABLE dbo.Formulario  
		   (idFormulario int identity(1,1) NOT NULL,
			GUIDFormulario varchar(40) unique Not NULL,
			nombreFormulario varchar(50) NOT NULL,  
			estado bit Not NULL,
			CONSTRAINT PK_Formulario PRIMARY KEY (idFormulario))

IF EXISTS(SELECT * FROM SYSOBJECTS WHERE XTYPE='U' AND NAME = 'TipoDetalle')
	BEGIN
		DROP TABLE TipoDetalle;
	END

	CREATE TABLE dbo.TipoDetalle  
		   (idTipoDetalle int identity(1,1) NOT NULL,
			nombreTipoDetalle varchar(25) Not NULL,
			CONSTRAINT PK_TipoDetalle PRIMARY KEY (idTipoDetalle))

	insert into TipoDetalle(nombreTipoDetalle) VALUES ('Dato');
	insert into TipoDetalle(nombreTipoDetalle) VALUES ('Im�gen');
	insert into TipoDetalle(nombreTipoDetalle) VALUES ('Video');

	
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE XTYPE='U' AND NAME = 'TipoConexion')
	BEGIN
		DROP TABLE TipoConexion;
	END

	CREATE TABLE dbo.TipoConexion  
		   (idTipoConexion int identity(1,1) NOT NULL,
			nombreTipoConexion varchar(20) unique Not NULL,
			CONSTRAINT PK_TipoConexion PRIMARY KEY (idTipoConexion))

	insert into TipoConexion(nombreTipoConexion) VALUES ('Formulario');
	insert into TipoConexion(nombreTipoConexion) VALUES ('Im�gen');
	insert into TipoConexion(nombreTipoConexion) VALUES ('Video');

IF EXISTS(SELECT * FROM SYSOBJECTS WHERE XTYPE='U' AND NAME = 'TipoDato')
	BEGIN
		DROP TABLE TipoDato;
	END

	CREATE TABLE dbo.TipoDato  
		   (idTipoDato int identity(1,1) NOT NULL,
			nombreTipoDato varchar(20) unique Not NULL,
			CONSTRAINT PK_TipoDato PRIMARY KEY (idTipoDato))

IF EXISTS(SELECT * FROM SYSOBJECTS WHERE XTYPE='U' AND NAME = 'Campo')
	BEGIN
		DROP TABLE Campo;
	END

	CREATE TABLE dbo.Campo  
		   (idCampo bigint identity(1,1) NOT NULL,
		   --Este nombre del campo ser� del campo identifica apiId de formulario
			nombreCampo varchar(15) Not NULL, 
			idTipoDato int Not NULL,
			description varchar(100) Not Null,
			CONSTRAINT PK_Campo PRIMARY KEY (idCampo),
			CONSTRAINT PK_TipoDatoCampo FOREIGN KEY (idTipoDato) references TipoDato(idTipoDato))

IF EXISTS(SELECT * FROM SYSOBJECTS WHERE XTYPE='U' AND NAME = 'Conexion')
	BEGIN
		DROP TABLE Conexion;
	END

	CREATE TABLE dbo.Conexion  
		   (idConexion int identity(1,1) NOT NULL,
			idTipoConexion int Not NULL,
			idFormulario int unique Not Null,
			nombreConexion varchar(25) unique NOT NULL, 
			fuente varchar(150) NOT NULL,
			usuarioFuente varchar(25) NOT NULL,
			contrasenaFuente varchar(25) NOT NULL,
			periodoSincronizacion int NOT NULL,
			descripcion varchar(255) NOT NULL,
			fechaActualizacion date default GETDATE(),
			CONSTRAINT PK_Conexion PRIMARY KEY (idConexion),
			CONSTRAINT FK_TipoConexion FOREIGN KEY (idTipoConexion) REFERENCES TipoConexion(idTipoConexion),
			CONSTRAINT FK_FormularioConexion FOREIGN KEY (idFormulario) REFERENCES Formulario(idFormulario))

IF EXISTS(SELECT * FROM SYSOBJECTS WHERE XTYPE='U' AND NAME = 'Detalle')
	BEGIN
		DROP TABLE Detalle;
	END

	CREATE TABLE dbo.Detalle  
		   (idDetalle bigint identity(1,1) NOT NULL,
			idConexion int Not NULL,
			urlDetalle varchar(150) NOT NULL,  
			idTipoDetalle int not NULL,
			CONSTRAINT PK_Detalle PRIMARY KEY (idDetalle),
			CONSTRAINT FK_TipoDetalle FOREIGN KEY (idTipoDetalle) REFERENCES TipoDetalle(idTipoDetalle),
			CONSTRAINT FK_FormularioDetalle FOREIGN KEY (idConexion) REFERENCES Conexion(idConexion))

IF EXISTS(SELECT * FROM SYSOBJECTS WHERE XTYPE='U' AND NAME = 'Estructura')
	BEGIN
		DROP TABLE Estructura;
	END

	CREATE TABLE dbo.Estructura 
		   (idEstructura int identity(1,1),
			idConexion int NOT NULL,
			idCampo bigint Not Null,
			CONSTRAINT PK_Estructura PRIMARY KEY (idEstructura),
			CONSTRAINT FK_ConexionEstructura FOREIGN KEY (idConexion) REFERENCES Conexion(idConexion),
			CONSTRAINT FK_CampoEstructura FOREIGN KEY (idCampo) REFERENCES Campo(idCampo))

IF EXISTS(SELECT * FROM SYSOBJECTS WHERE XTYPE='U' AND NAME = 'Usuario')
	BEGIN
		DROP TABLE Usuario;
	END

	CREATE TABLE dbo.Usuario  
		   (idUsuario bigint identity(1,1) NOT NULL,
			GUIDUsuario varchar(40) Not Null,
			nombre varchar(50) Not Null,
			CONSTRAINT PK_Usuario PRIMARY KEY (idUsuario))

IF EXISTS(SELECT * FROM SYSOBJECTS WHERE XTYPE='U' AND NAME = 'TipoLocacion')
	BEGIN
		DROP TABLE TipoLocacion;
	END

	CREATE TABLE dbo.TipoLocacion  
		   (idTipoLocacion int identity(1,1) NOT NULL,
			GUIDTipoLocation varchar(40) Not Null,
			nombre varchar(50) Not Null,
			estado bit Not Null,
			CONSTRAINT PK_TipoLocacion PRIMARY KEY (idTipoLocacion))

IF EXISTS(SELECT * FROM SYSOBJECTS WHERE XTYPE='U' AND NAME = 'Locacion')
	BEGIN
		DROP TABLE Locacion;
	END

	CREATE TABLE dbo.Locacion  
		   (idLocacion bigint identity(1,1) NOT NULL,
			GUIDLocation varchar(40) Not Null,
			nombre varchar(30) Not Null,
			nombreContacto varchar(30) Not Null,
			email varchar(50) Not Null,
			telefono varchar(50) Not Null,
			fax varchar(50) Not Null,
			direccion varchar(50) Not Null,
			ciudad varchar(20) Not Null,
			departamento varchar(20) Not Null,
			pais varchar(20) Not Null,
			latitud varchar(20) Not Null,
			longitud varchar(20) Not Null,
			tagUID varchar(20) Not Null,
			idTipoLocacion int Not Null,
			fechaActualizacion datetime Not Null,
			CONSTRAINT PK_Locacion PRIMARY KEY (idLocacion),
			CONSTRAINT FK_TipoLocacion FOREIGN KEY (idTipoLocacion) REFERENCES TipoLocacion(idTipoLocacion))

IF EXISTS(SELECT * FROM SYSOBJECTS WHERE XTYPE='U' AND NAME = 'Formulario1')
	BEGIN
		DROP TABLE Formulario1;
	END

	CREATE TABLE dbo.Formulario1  
		   (idFormulario1 bigint identity(1,1) NOT NULL,
			idLocacion bigint Not NULL,
			idFormulario int Not Null,
			fechaCreacion varchar(40) Not Null,
			fechaActualizacion varchar(40) Not Null,
			idUsuario bigint Not Null,
			FECHA varchar(30) Not Null,
			HORA varchar(30) Not Null,
			TRAYEC varchar(30) Not Null,
			OTROTRAY varchar(50) Not Null,
			NOVEDAD varchar(50) Not Null,
			DESCRIP varchar(29) Not Null,
			CONSTRAINT PK_Formulario1 PRIMARY KEY (idFormulario1),
			CONSTRAINT FK_LocacionFormulario1 FOREIGN KEY (idLocacion) REFERENCES Locacion(idLocacion),
			CONSTRAINT FK_FormularioFormulario1 FOREIGN KEY (idFormulario) REFERENCES Formulario(idFormulario),
			CONSTRAINT FK_UsuarioFormulario1 FOREIGN KEY (idUsuario) REFERENCES Usuario(idUsuario))
 
 ----------------------------------------------------Procedimientos almacenadois------------------------------------------------------
 USE [minasDB]
GO

/****** Object:  StoredProcedure [dbo].[AddColumna]    Script Date: 12/09/2017 9:08:39 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[AddColumna]
	 @NombreTabla varchar(25),
	 @NombreColumna varchar(25),
	 @TipoColumna varchar(15),
	 @Obligatorio varchar(15)


AS
BEGIN
	

   DECLARE @SQLString NVARCHAR(MAX)
   SET @SQLString = 'ALTER TABLE '+ @NombreTabla + ' ADD '+@NombreColumna+' '+@TipoColumna+' '+@Obligatorio+' ;'

   EXEC (@SQLString)

		
END





GO

USE [minasDB]
GO

/****** Object:  StoredProcedure [dbo].[AddRegistro]    Script Date: 12/09/2017 9:08:50 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







CREATE PROCEDURE [dbo].[AddRegistro]

     @NombreTabla nvarchar(max),
	 @Values nvarchar(max),
	 @Columns nvarchar(max)

AS
BEGIN
	
   DECLARE @SQLString NVARCHAR(MAX)
   SET @SQLString = 'INSERT INTO '+ @NombreTabla +  +'('+@Columns+') VALUES('+@Values+') ;'



   EXEC (@SQLString)

		
END






GO
USE [minasDB]
GO

/****** Object:  StoredProcedure [dbo].[AddRegla]    Script Date: 12/09/2017 9:09:01 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[AddRegla]

	 @NombreTabla varchar(25),
	 @NombreColumna varchar(25),
	 @TablaReferencia varchar(25),
	 @NombreColumnaReferencia varchar(25)

AS
BEGIN
	
   DECLARE @SQLString NVARCHAR(MAX)
   SET @SQLString = 'ALTER TABLE '+ @NombreTabla + '  WITH CHECK ADD FOREIGN KEY( ['+@NombreColumna+']) REFERENCES [dbo].['+@TablaReferencia+'] (['+@NombreColumnaReferencia+']) ;'



   EXEC (@SQLString)

		
END





GO

USE [minasDB]
GO

/****** Object:  StoredProcedure [dbo].[ColumnaClavePrimaria]    Script Date: 12/09/2017 9:09:12 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[ColumnaClavePrimaria]
	 @NombreTabla varchar(25),
	 @NombreColumna varchar(25)

AS
BEGIN

SELECT TABLE_NAME, COLUMN_NAME, CONSTRAINT_NAME
FROM information_schema.key_column_usage
WHERE  TABLE_NAME =  @NombreTabla and COLUMN_NAME = @NombreColumna and CONSTRAINT_NAME LIKE 'PK%'
		
END






GO

USE [minasDB]
GO

/****** Object:  StoredProcedure [dbo].[ColumnaExiste]    Script Date: 12/09/2017 9:09:31 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[ColumnaExiste]
	 @NombreTabla varchar(25),
	 @NombreColumna varchar(25)


AS
BEGIN

Select * from( 
	
	SELECT OBJ.id AS id_tabla, 
        ROW_NUMBER() OVER (ORDER BY COL.colid) AS id_columna, 
        COL.name AS columna, 
        TYP.name AS Tipo, 
        --Por algun motivo los nvarchar dan el doble de la longitud
        Longitud = CASE TYP.name 
            WHEN 'nvarchar' THEN COL.LENGTH/2
            WHEN 'varchar' THEN COL.LENGTH/2
            ELSE COL.LENGTH
            END,
        COL.xprec AS PRECISION, 
        COL.xscale AS escala, 
        COL.isnullable AS Isnullable, 
        FK.constid AS id_fk, 
        OBJ2.name AS table_derecha, 
        COL2.name 
        --COL.*
    FROM dbo.syscolumns COL
    JOIN dbo.sysobjects OBJ ON OBJ.id = COL.id
    JOIN dbo.systypes TYP ON TYP.xusertype = COL.xtype
    --left join dbo.sysconstraints CON on CON.colid = COL.colid
    LEFT JOIN dbo.sysforeignkeys FK ON FK.fkey = COL.colid AND FK.fkeyid=OBJ.id
    LEFT JOIN dbo.sysobjects OBJ2 ON OBJ2.id = FK.rkeyid
    LEFT JOIN dbo.syscolumns COL2 ON COL2.colid = FK.rkey AND COL2.id = OBJ2.id
    WHERE OBJ.name = @NombreTabla  AND (OBJ.xtype='U' OR OBJ.xtype='V')
	) as TableColumn

	where columna = @NombreColumna
		
END




GO




USE [minasDB]
GO

/****** Object:  StoredProcedure [dbo].[CrearTabla]    Script Date: 12/09/2017 9:09:51 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[CrearTabla]
	 @NombreTabla varchar(25)

AS
BEGIN
	CREATE TABLE [dbo].[@NombreTabla](
	[Codigo] [int] IDENTITY(1,1) NOT NULL,

	PRIMARY KEY CLUSTERED 
	([Codigo] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
    )
			
END



GO

USE [minasDB]
GO

/****** Object:  StoredProcedure [dbo].[RegistroExiste]    Script Date: 12/09/2017 9:10:00 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[RegistroExiste]
	 @NombreTabla varchar(25),
	 @NombreColumna varchar(25),
	 @IdPK varchar(25)



AS
BEGIN
	
   Declare @Result int
   DECLARE @SQLString NVARCHAR(MAX)
    SET @SQLString = 'SELECT @Result = Count(*) FROM '+@NombreTabla+' WHERE '+@NombreColumna+' = '+@IdPK+';'


    execute sp_executesql @SQLString, N'@Result int OUTPUT', @Result output
    select @Result as Resultado
END


GO

USE [minasDB]
GO

/****** Object:  StoredProcedure [dbo].[TablaExiste]    Script Date: 12/09/2017 9:10:10 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[TablaExiste]
	 @NombreTabla varchar(25)

AS
BEGIN
	
	SELECT OBJ.id AS id_tabla, 
        ROW_NUMBER() OVER (ORDER BY COL.colid) AS id_columna, 
        COL.name AS columna, 
        TYP.name AS Tipo, 
        --Por algun motivo los nvarchar dan el doble de la longitud
        Longitud = CASE TYP.name 
            WHEN 'nvarchar' THEN COL.LENGTH/2
            WHEN 'varchar' THEN COL.LENGTH/2
            ELSE COL.LENGTH
            END,
        COL.xprec AS PRECISION, 
        COL.xscale AS escala, 
        COL.isnullable AS Isnullable, 
        FK.constid AS id_fk, 
        OBJ2.name AS table_derecha, 
        COL2.name 
        --COL.*
    FROM dbo.syscolumns COL
    JOIN dbo.sysobjects OBJ ON OBJ.id = COL.id
    JOIN dbo.systypes TYP ON TYP.xusertype = COL.xtype
    --left join dbo.sysconstraints CON on CON.colid = COL.colid
    LEFT JOIN dbo.sysforeignkeys FK ON FK.fkey = COL.colid AND FK.fkeyid=OBJ.id
    LEFT JOIN dbo.sysobjects OBJ2 ON OBJ2.id = FK.rkeyid
    LEFT JOIN dbo.syscolumns COL2 ON COL2.colid = FK.rkey AND COL2.id = OBJ2.id
    WHERE OBJ.name = @NombreTabla AND (OBJ.xtype='U' OR OBJ.xtype='V')

		
END
GO
USE [minasDB]
GO

/****** Object:  StoredProcedure [dbo].[ChangeNameColumn]    Script Date: 13/09/2017 10:36:30 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ChangeNameColumn]
	
	 @TableName varchar(25),
	 @OldColumnName varchar(25),
	 @NewColumnName varchar(25)


AS
BEGIN
	
	DECLARE @StringChange varchar(50);
	set @StringChange =  @TableName+'.'+@OldColumnName;


	EXEC sp_RENAME   @StringChange , @NewColumnName, 'COLUMN'

		
END




GO